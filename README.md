# Ansible-Pull Setup

## 1. Configuring the Runner Account
-----
The runner account is the user account created to run the anisble-pull system. The following script will install the runner account

```bash
wget -q https://gitlab.com/eITPublic/AnsiblePullSetup/raw/master/runner/create-runner.sh -O-| sudo bash
```

To install the runner account using cURL run the follwing script

```bash
curl -fsS https://gitlab.com/eITPublic/AnsiblePullSetup/raw/master/runner/create-runner.sh | sudo bash
```

## 2. Installing Ansible
-----
Ansible can be installed through running the following script

```bash
wget -q https://gitlab.com/eITPublic/AnsiblePullSetup/raw/master/Install-Ansible.sh -O-| sudo bash
```
or
```bash
curl -fsS https://gitlab.com/eITPublic/AnsiblePullSetup/raw/master/Install-Ansible.sh | sudo bash
```

## 3. Configuring Pull